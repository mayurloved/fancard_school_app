import {
  Component,
  OnInit
} from '@angular/core';
import {
  BarcodeScanner
} from '@ionic-native/barcode-scanner/ngx';

import {
  NavController, AlertController, Platform
} from '@ionic/angular';
import { APIconstants } from '../../shared/constants';
import { MainService } from '../../shared/main/main.service';
import { HttpService } from '../../shared/http/http.service';
import { ActivatedRoute, Router, NavigationExtras  } from '@angular/router';

@Component({
  selector: 'app-scan-qr-code',
  templateUrl: './scan-qr-code.page.html',
  styleUrls: ['./scan-qr-code.page.scss'],
})
export class ScanQrCodePage implements OnInit {

  isIos: any = false;
  eventId: any;
  fanCount: any;
  foo: any;
  constructor(private barcodeScanner: BarcodeScanner,
    public navCtrl: NavController,
    public alertCtrl : AlertController,
    public plt: Platform,
    public http: HttpService,
    public mainService: MainService,
    public activeroute: ActivatedRoute,
    public router: Router
  ) {
    if (this.plt.is('ios')) {
      this.isIos = true;
    }
  }

  ngOnInit() {
    this.activeroute.queryParams.subscribe(params => {
      this.eventId = params['ID'];
    });
    this.fanCount = this.router.getCurrentNavigation().extras.state.count;
  }


  openQRScanner() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.mainService.showLoader();
      if (barcodeData.cancelled) {
        this.navCtrl.navigateForward(['/tabs/upComingEvents'])
      } else if (barcodeData.text != '' && !(barcodeData.cancelled)) {
        let params = new FormData();
        params.append('QRString', barcodeData.text);
        params.append('EventId', this.eventId);
        this.http.postApiData(APIconstants.SCAN_QR_CODE, params)
          .subscribe(data => {
            console.log(data);
            let res: any = data
            this.mainService.dismissLoading();
            if (res.status) {
              this.SucesspresentAlert()
              this.fanCount = res.detail[0].attended_total;
              // this.navCtrl.navigateForward(['/tabs/upComingEvents'])
            } else {
              this.FaliurepresentAlert();
            }
          }, err => {
            this.mainService.dismissLoading();
            this.mainService.showToast('Something went wrong.try again.');
          })
       
      }else{
      this.mainService.dismissLoading();
       this.FaliurepresentAlert();
      }
    },err=>{
        this.mainService.dismissLoading();
        this.FaliurepresentAlert();
    });
  }



  onBackClick() {
    // let navigationExtras: NavigationExtras = { state: { foo: this.foo } };
    this.navCtrl.navigateForward(['/tabs/upComingEvents'])
  }

  async FaliurepresentAlert(){
    let alert = await this.alertCtrl.create({ 
      subHeader: 'Sorry,',
      message: 'Your QR code has already been scanned at this event.',
      buttons:[],
      cssClass: 'custom-faliure-alert',
    })
    await alert.present();
  }

  async SucesspresentAlert(msg?) {
    let alert = await this.alertCtrl.create({
      subHeader: 'Success',
      message: msg,
      buttons: [],
      cssClass: 'custom-sucess-alert',
      backdropDismiss: true
    })
    await alert.present();
  }

}