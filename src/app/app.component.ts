import { Component } from '@angular/core';

import {  Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { HttpService } from '../shared/http/http.service';
import { MainService } from '../shared/main/main.service';
import {  APIconstants } from '../shared/constants';
import { AuthService } from '../shared/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  pltName:any;
  loginData: any;
  constructor(
    private plt: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,

    public appVersion : AppVersion,

    public http : HttpService,

    public main: MainService,

    public auth : AuthService,

    public navCtrl : NavController 
    
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.plt.ready().then(() => {
      this.splashScreen.hide();
      if(this.plt.is('ios')){
        this.pltName = 'ios';
        this.statusBar.styleDefault();

      }else{
        this.pltName = 'android';
        this.statusBar.styleBlackOpaque();
      }

      /*----Check login session and manage redirection--------------*/
      this.loginData = this.auth.getUser();
      if (this.loginData) {
        this.navCtrl.navigateRoot('/tabs');
        
      } else {
        this.navCtrl.navigateRoot('/auth/login');
        
      }

      this.getAppversion();
    });
  }

  getAppversion(){

    /*----NOTE:HERE WE CALL API IN  IN GET VERSION NUMBER PLUGIN METHOD BECAUSE OF APP VERSION PLUGIN TAKE A TIME TO THROW IT'S RESPONSE.--------------*/
    this.appVersion.getVersionNumber().then((number)=>{
      this.main.appVersion = number
      let url = APIconstants.INIT_APP_VERSION + '/app_version=' + this.main.appVersion + '?type=' + this.pltName;
      this.http.getApiData(url)
        .subscribe(data => {
        })
    })   
  }

}
