import { Component, ViewChildren, QueryList, ViewChild} from '@angular/core';
import { Platform, IonRouterOutlet, NavController, IonTabs } from '@ionic/angular';
import { Router } from '@angular/router';
import { MainService } from '../../shared/main/main.service';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  @ViewChild('myTabs',{static:true}) tabRef: IonTabs;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  selectTab = 'upComingEvents';

  isIOS:any = false;
  constructor(
    public plt : Platform,
    public router : Router,

    public mainService : MainService,

    public navCtrl : NavController
  ) {
    this.plt.ready().then(() => {
      this.mainService.showLoader();
      if(this.plt.is('ios')){
        this.isIOS = true;
      }
    })
  }

  tabChange(e) {
    this.selectTab = e.tab;
  }
}
