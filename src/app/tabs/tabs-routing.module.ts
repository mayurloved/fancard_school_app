import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'upComingEvents',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../upComing-events/upComingEvents.module').then(m => m.UpComingEventsPageModule)
          }
        ]
      },
      {
        path: 'pastEvents',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../past-events/pastEvents.module').then(m => m.PastEventsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/upComingEvents',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
