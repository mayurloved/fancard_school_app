import { Component, ViewChildren, QueryList } from '@angular/core';
import { NavController, Platform, IonRouterOutlet } from '@ionic/angular';
import { APIconstants, constants } from 'src/shared/constants';
import { HttpService } from 'src/shared/http/http.service';
import { AuthService } from 'src/shared/auth/auth.service';
import { MainService } from 'src/shared/main/main.service';
import { NavigationExtras } from '@angular/router';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-upComingEvents',
  templateUrl: 'upComingEvents.page.html',
  styleUrls: ['upComingEvents.page.scss']
})
export class UpcomingEventsPage {


  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  isIos: any = false;
  upcomingEvents: any = [];
  userDetails: any;
  schoolImage: any;
  foo: any;
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public navCtrl: NavController,
    public httpService: HttpService,
    public plt: Platform,
    public authService: AuthService,
    public mainService: MainService
  ) {

    this.route.queryParams.subscribe(params => {
      this.fetchUpcomingEvents();
    });

    if (this.plt.is('ios')) {
      this.isIos = true;
    }
    this.userDetails = this.authService.getUser();
    this.plt.ready().then(() => {
      document.addEventListener('backbutton', async () => {
        if (this.router.url == '/tabs/upComingEvents' && this.userDetails) {
          if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
            this.lastTimeBackPress = new Date().getTime();
            this.mainService.showToast('press again back button to exit app.')
          } else {
            navigator['app'].exitApp();
          }
        }
      })
    });
  }

  async ngOnInit() {
    this.schoolImage = this.schoolImage = this.userDetails.SchoolImage ? constants.IMAGE_URL + this.userDetails.SchoolImage : '';
  }

  ionViewDidEnter() {
    this.fetchUpcomingEvents();
  }
  onProfileClick() {
    this.navCtrl.navigateForward(['/auth/profile'])
  }

  fetchUpcomingEvents() {
    this.mainService.showLoader();
    let url = APIconstants.UPCOMING_EVENTS + this.userDetails.SId;
    this.httpService.getApiData(url)
      .subscribe(data => {
        this.mainService.dismissLoading();
        const res: any = data;
        this.upcomingEvents = res.eventdata;
      }, err => {
        console.log(err);
      })
  }

  openQRScanner(id, fanCount) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        ID: id
      },
      state: {
        count: fanCount
      }
    };
    this.navCtrl.navigateForward(['/scan-qr-code'], navigationExtras)
  }
}
