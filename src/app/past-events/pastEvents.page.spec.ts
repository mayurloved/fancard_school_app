import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { pastEventsPage } from './pastEvents.page';

describe('pastEventsPage', () => {
  let component: pastEventsPage;
  let fixture: ComponentFixture<pastEventsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [pastEventsPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(pastEventsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
