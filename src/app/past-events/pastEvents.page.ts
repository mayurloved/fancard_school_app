import { Component, ViewChildren, QueryList } from '@angular/core';
import { NavController, Platform, IonRouterOutlet } from '@ionic/angular';
import { APIconstants, constants } from 'src/shared/constants';
import { HttpService } from 'src/shared/http/http.service';
import { AuthService } from 'src/shared/auth/auth.service';
import { MainService } from 'src/shared/main/main.service';
import { NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pastEvents',
  templateUrl: 'pastEvents.page.html',
  styleUrls: ['pastEvents.page.scss']
})
export class pastEventsPage {

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  isIos: any = false;
  pastEvents: any = [];
  userDetails: any;
  schoolImage: any;
  constructor(
    public router: Router,
    public navCtrl: NavController,
    public httpService: HttpService,
    public plt: Platform,
    public authService: AuthService,
    public mainService: MainService

  ) {
    if (this.plt.is('ios')) {
      this.isIos = true;
    }
    this.userDetails = this.authService.getUser();
    this.schoolImage = this.schoolImage = this.userDetails.SchoolImage ? constants.IMAGE_URL + this.userDetails.SchoolImage : '';

    this.plt.ready().then(() => {
      document.addEventListener('backbutton', async () => {
        if (this.router.url == '/tabs/pastEvents' && this.userDetails) {
          this.navCtrl.navigateForward(['/tabs/upComingEvents'])
        }
      })
    });
  }

  async ngOnInit() {
  }

  onProfileClick() {
    this.navCtrl.navigateForward(['/auth/profile'])
  }

  ionViewWillEnter() {
    this.fetchPastEvents();
  }

  fetchPastEvents() {
    this.mainService.showLoader();
    let url = APIconstants.PAST_EVENTS + this.userDetails.SId;
    this.httpService.getApiData(url)
      .subscribe(data => {
        this.mainService.dismissLoading();
        const res: any = data;
        this.pastEvents = res.eventdata;
      }, err => {
        console.log(err);
      })
  }
}
