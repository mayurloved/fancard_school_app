import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  loading: any;

  appVersion:any;

  constructor
    (
      public loadCtrl: LoadingController,
      public toastCtrl: ToastController,
      public alrtCtrl: AlertController,
    
      
  ) { }

  async showLoader() {
    this.loading = await this.loadCtrl.create({
      spinner: null,
      duration: 1500,
      message: `
      <div class="lds-circle"><div></div></div>`,
      cssClass: 'custom-class-custom-loading'
    });
    return await this.loading.present();
  }

  async dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


  async showToast(message: any) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentAlert(subHeader, message) {
    const alert = await this.alrtCtrl.create({

      subHeader: subHeader,
      message: message,
      buttons: ['OK', 'Cancel']
    });

    await alert.present();
  }




}