import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    public http: HttpClient
  ) { }

  getHeader() {
    const httpOptions = {
      headers: new HttpHeaders({
        key: "fancard123*#*"
      })
    };

    return httpOptions;

  }

  getApiData(url) {
    let options = this.getHeader();
    return this.http.get(constants.BASE_URL + url, options );
  }

  
  postApiData(url, body){
    let options = this.getHeader();
    return this.http.post(constants.BASE_URL + url , body, options);
  }

}
