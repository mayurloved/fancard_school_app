export class constants {
    public static BASE_URL = "https://www.fancard.app/admin-panel/Fancardschool/";
    public static IMAGE_URL = 'https://www.fancard.app/admin-panel/assets/images/Card/'

}
export class APIconstants {
    public static INIT_APP_VERSION = "Init";
    public static SIGNIN = "SignIN";
    public static FORGOT_PASS = "ForgotPassword";
    public static CHANGE_PASS = "ChangePassword";
    public static EDIT_PROFILE = "ProfileManagement/";
    public static UPCOMING_EVENTS = "UpcomingEvent/";
    public static PAST_EVENTS = "PastEvent/";
    public static SCAN_QR_CODE = "QRScan";
}