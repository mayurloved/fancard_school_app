import { Injectable } from '@angular/core';
import { MainService } from '../main/main.service';


@Injectable({
    providedIn: 'root'
})

export class AuthService {

    user: any;
    storageData: any;

    constructor(
        public mainService: MainService
    ) {
        this.storageData =JSON.parse(window.localStorage.getItem('fancard-school-data'));
        if (this.storageData) {
            this.user = this.storageData;
        }
    }

    login(user) {
        this.user = user;
        return window.localStorage.setItem('fancard-school-data', JSON.stringify(user));
    }

    logout() {
        this.mainService.showLoader();
        this.user = null;
        this.mainService.dismissLoading();
        return window.localStorage.removeItem('fancard-school-data');

    }

    getUser() {
        return this.user;
    }

    
}
